output_horizontal =
  {
    filename = "__modular_storage__/graphics/entity/output/output.png",
    priority = "extra-high",
    width = 40,
    height = 40,
    frame_count = 32,
    hr_version =
    {
      filename = "__modular_storage__/graphics/entity/output/hr-output.png",
      priority = "extra-high",
      width = 80,
      height = 80,
      frame_count = 32,
      line_length = 16,
      scale = 0.5
    }
  }
output_vertical =
  {
    filename = "__modular_storage__/graphics/entity/output/output.png",
    priority = "extra-high",
    width = 40,
    height = 40,
    frame_count = 32,
    y = 40,
    hr_version =
    {
      filename = "__modular_storage__/graphics/entity/output/hr-output.png",
      priority = "extra-high",
      width = 80,
      height = 80,
      frame_count = 32,
      line_length = 16,
      y = 160,
      scale = 0.5
    }
  }
output_ending_top =
  {
    filename = "__modular_storage__/graphics/entity/output/output.png",
    priority = "extra-high",
    width = 40,
    height = 40,
    frame_count = 32,
    y = 80,
    hr_version =
    {
      filename = "__modular_storage__/graphics/entity/output/hr-output.png",
      priority = "extra-high",
      width = 80,
      height = 80,
      frame_count = 32,
      line_length = 16,
      y = 320,
      scale = 0.5
    }
  }
output_ending_bottom =
  {
    filename = "__modular_storage__/graphics/entity/output/output.png",
    priority = "extra-high",
    width = 40,
    height = 40,
    frame_count = 32,
    y = 120,
    hr_version =
    {
      filename = "__modular_storage__/graphics/entity/output/hr-output.png",
      priority = "extra-high",
      width = 80,
      height = 80,
      frame_count = 32,
      line_length = 16,
      y = 480,
      scale = 0.5
    }
  }
output_ending_side =
  {
    filename = "__modular_storage__/graphics/entity/output/output.png",
    priority = "extra-high",
    width = 40,
    height = 40,
    frame_count = 32,
    y = 160,
    hr_version =
    {
      filename = "__modular_storage__/graphics/entity/output/hr-output.png",
      priority = "extra-high",
      width = 80,
      height = 80,
      frame_count = 32,
      line_length = 16,
      y = 640,
      scale = 0.5
    }
  }
