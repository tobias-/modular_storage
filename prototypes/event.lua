data:extend({
  {
    type = "custom-input",
    name = "change-output-item-1",
    key_sequence = "CONTROL + A",
    consuming = "script-only"
  },
  {
    type = "custom-input",
    name = "change-output-item-2",
    key_sequence = "CONTROL + E",
    consuming = "script-only"
  },
  {
    type = "custom-input",
    name = "change-interface-items",
    key_sequence = "CONTROL + Z",
    consuming = "script-only"
  }
})