input_horizontal =
  {
    filename = "__modular_storage__/graphics/entity/input/input.png",
    priority = "extra-high",
    width = 40,
    height = 40,
    frame_count = 32,
    hr_version =
    {
      filename = "__modular_storage__/graphics/entity/input/hr-input.png",
      priority = "extra-high",
      width = 80,
      height = 80,
      frame_count = 32,
      line_length = 16,
      scale = 0.5
    }
  }
input_vertical =
  {
    filename = "__modular_storage__/graphics/entity/input/input.png",
    priority = "extra-high",
    width = 40,
    height = 40,
    frame_count = 32,
    y = 40,
    hr_version =
    {
      filename = "__modular_storage__/graphics/entity/input/hr-input.png",
      priority = "extra-high",
      width = 80,
      height = 80,
      frame_count = 32,
      line_length = 16,
      y = 160,
      scale = 0.5
    }
  }
input_starting_top =
  {
    filename = "__modular_storage__/graphics/entity/input/input.png",
    priority = "extra-high",
    width = 40,
    height = 40,
    frame_count = 32,
    y = 200,
    hr_version =
    {
      filename = "__modular_storage__/graphics/entity/input/hr-input.png",
      priority = "extra-high",
      width = 80,
      height = 80,
      frame_count = 32,
      line_length = 16,
      y = 800,
      scale = 0.5
    }
  }
input_starting_bottom =
  {
    filename = "__modular_storage__/graphics/entity/input/input.png",
    priority = "extra-high",
    width = 40,
    height = 40,
    frame_count = 32,
    y = 240,
    hr_version =
    {
      filename = "__modular_storage__/graphics/entity/input/hr-input.png",
      priority = "extra-high",
      width = 80,
      height = 80,
      frame_count = 32,
      line_length = 16,
      y = 960,
      scale = 0.5
    }
  }
input_starting_side =
  {
    filename = "__modular_storage__/graphics/entity/input/input.png",
    priority = "extra-high",
    width = 40,
    height = 40,
    frame_count = 32,
    y = 280,
    hr_version =
    {
      filename = "__modular_storage__/graphics/entity/input/hr-input.png",
      priority = "extra-high",
      width = 80,
      height = 80,
      frame_count = 32,
      line_length = 16,
      y = 1120,
      scale = 0.5
    }
  }